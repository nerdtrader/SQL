set nocount on
set transaction isolation level read uncommitted

declare
	@find_text varchar(max)

select @find_text = 'mfb06'

select distinct 
	so.xtype
,	so.name
from 
	syscomments sc
	inner join sysobjects so 
		on sc.id = so.id
where 
	sc.text like '%'+@find_text+'%'
order by 
	so.xtype
,	so.name