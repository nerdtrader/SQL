USE msdb;

EXEC sp_help_jobhistory 
	@job_name       = 'JobName'
,	@start_run_date = 20170901
,	@end_run_date   = 20171231
,	@mode           = 'FULL';