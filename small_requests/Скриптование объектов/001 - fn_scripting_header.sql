--<DB>
use master
go
--</DB>
if object_id('[dbo].[fn_scripting_header]', 'FN') is null
	exec('create function [dbo].[fn_scripting_header]() returns int as begin return (1) end')
go

alter function [dbo].[fn_scripting_header]
(
	@db varchar(255)
)
	returns varchar(max)
as
begin
	-------------------------------------------------------------------------------------------------------------------------
	-- ���������� ����� ��� �������� �������
	-------------------------------------------------------------------------------------------------------------------------
	declare 
		@result varchar(max)

	select 
		@result = replace
					(
'	
--<CREATE_SET>	
-----------------------------------------------------------------------------------------
set nocount on
set quoted_identifier, ansi_nulls, ansi_warnings, arithabort, concat_null_yields_null, ansi_padding on
set numeric_roundabort off
set transaction isolation level read uncommitted
set xact_abort on
-----------------------------------------------------------------------------------------
--</CREATE_SET>	
go

--<DB>
use <database>
go
--</DB>
'				
						,'<database>'
						,@db
					)
	return @result

end