--<DB>
use master
go
--</DB>
	
if object_id('[dbo].[fn_scripting_header_type]', 'FN') is null
	exec('create function [dbo].[fn_scripting_header_type]() returns int as begin return (1) end')
go

alter function [dbo].[fn_scripting_header_type]
(
	@name     varchar(255)
,	@db       varchar(255)	
,	@type     varchar(255)
,	@add_info xml = null
)
	returns varchar(max)
as
begin
	-------------------------------------------------------------------------------------------------------------------------
	-- ���������� ����� ��� �������� ������� ������� ���� @type
	-- @type = sys.objects.type
	-------------------------------------------------------------------------------------------------------------------------
	declare 
		@result varchar(max)
	select 
		@result = dbo.fn_scripting_header(@db)
				+ case
					when @type = 'P' then
						replace(
'	
if object_id(''<name>'', ''P'') is null
	exec(''create procedure <name> as begin return -1 end'')
go
'				
							,'<name>'
							,@name
						)
					when @type = 'IF' then
						replace(
'	
if object_id(''<name>'', ''IF'') is null
	exec(''create function <name>() returns table as return select 0 i'')
go
'				
							,'<name>'
							,@name
						)
					when @type = 'V' then
						replace(
'	
if object_id(''<name>'', ''V'') is null
	exec(''create view <name> as select null n'')
go
'				
							,'<name>'
							,@name
						)
					when @type = 'TR' then
						replace(replace(
'	
if object_id(''<name>'', ''TR'') is null
	exec(''create trigger <name> on [dbo].[<table_name>] for insert as begin return -1 end'')
go
'				
							,'<name>',@name),'<table_name>',@add_info.value('parent_name[1]','varchar(255)'))
					when @type = 'FN' then
						replace(
'	
if object_id(''<name>'', ''FN'') is null
	exec(''create function <name>() returns int as begin return (1) end'')
go
'				
							,'<name>',@name)
					when @type = 'TF' then
						replace(
'	
if object_id(''<name>'', ''TF'') is null
	exec(''create function <name>() returns @r table(i int) as begin insert into @r values(1) return end'')
go
'				
							,'<name>',@name)
			end

	return @result
	
end