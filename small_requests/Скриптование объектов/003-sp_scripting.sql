use master
go
if object_id('[dbo].[sp_scripting]', 'P') is null
	exec('create procedure [dbo].[sp_scripting] as begin return -1 end')
go
alter procedure [dbo].[sp_scripting]
	@p_object_name varchar(255)
as
begin
	set nocount on
	set transaction isolation level read uncommitted

	declare
		 @type          varchar(255) = null
		,@db            varchar(255) = null
		,@add_info      xml          = null
		,@p_script      varchar(max) = null
		,@replace       varchar(max) = null
		,@replacement   varchar(max) = null 
		,@sql           nvarchar(4000)
		,@script_all    varchar(max)
		,@script_header varchar(max)
		,@count         int
		,@DotIndex      int
		,@SchemaName    varchar(255)
		,@Name          varchar(255) = replace(replace(@p_object_name,']',''),'[','')
		,@FullName      varchar(255)
		
	select
		@db = isnull(@db, db_name())

	select
		@DotIndex = charindex('.', @Name)

	if @DotIndex > 0 
	begin
		set @SchemaName = left(@Name,  @DotIndex-1)
		set @Name       = right(@Name, len(@Name)-@DotIndex)
	end
	else 
		set @SchemaName = 'dbo'

	set @FullName= QUOTENAME(@SchemaName)+'.'+QUOTENAME(@Name)

	-- �������� ������ �������, �������� ������ create �� alter
	select 
		@sql 
			= replace(replace(
	'
	select 
	@script_all = stuff( 
			M.definition
		,charindex(''create'',M.definition)
		,len(''create'') 
		,''alter''
	)
	,@type = O.type
	,@add_info = (
		select
			P.name	parent_name
		for xml path('''')
	)
	,@count = count(*) over(partition by 1)
	from 
	<DATABASE>.sys.sql_modules			M
	inner join <DATABASE>.sys.objects	O on O.object_id = M.object_id
	left  join <DATABASE>.sys.triggers	T on T.object_id = M.object_id
	left  join <DATABASE>.sys.schemas	S on O.schema_id = S.schema_id
	left  join <DATABASE>.sys.objects	P on P.object_id = T.parent_id
	where
	O.name							= ''<NAME>''
	and S.name						= ''<SCHEMA>''			
	and isnull(T.parent_class, 1)	= 1	-- ��������� ������ ��������� ��������
	'
				,'<DATABASE>', @db), '<NAME>', @Name)
			
	set @sql = replace(@sql, '<SCHEMA>', @SchemaName)

	exec sp_executesql
			@sql
			,N'@script_all varchar(max) output, @type varchar(255) output, @add_info xml output, @count int output'
			,@p_script		output
			,@type		output
			,@add_info	output
			,@count			output
	

	
		if @count > 1 
		begin
			raiserror('������� ������ ������ ����������� �������', 16, 5)	
		end
	
		if isnull(@count, 0) = 0
		begin
			raiserror('�� ������� ����������� �������', 16, 5)	
		end
		
		-- ��������� � ������� �����
		select
			@script_header = dbo.fn_scripting_header_type(@FullName, isnull(@db, db_name()), @type, @add_info)
		
		if @script_header is null
			raiserror('��� ������ ���� %s �� ��������� ��������� �������', 16, 5, @type)
		
		select
			@p_script= @script_header + @p_script

		if @replace is not null
			and @replacement is not null
		begin
			select
				@p_script = replace(@p_script, @replace, @replacement)
		end

		-- �������� �� ������ ����� 13 10, ���� ����� ������� ��������, �� ������ ����� ������ � � ������ � � �����
			;with q as (
				select 
					1 n
					,substring(
						@p_script
						, 1
						, isnull(
							nullif(
								charindex(char(13),@p_script)
								,0)-1
							,datalength(@p_script)
						)
					) s
					,substring(
						@p_script
						,nullif(charindex(char(13),@p_script),0) + 2
						,datalength(@p_script)
					) r
				union all 
				select 
					 n + 1 n
					,substring(
						r
						, 1
						, isnull(
							nullif(
								charindex(char(13),r)
								,0)-1
							,datalength(r)
						)
					) s
					,substring(
						r
						,nullif(charindex(char(13),r),0) + 2
						,datalength(r)
					) r
				from 
					q 
				where 
					r is not null
			)
			select s " " from q order by n option(maxrecursion 0)
end