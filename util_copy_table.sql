alter procedure [dbo].[util_copy_table_fast]
	 @p_xml			xml								-- xml, ���. ������������	
	,@p_debug		int = 0
as
begin

	-----------------------------------------------------------------------------------------
	set nocount on
	set transaction isolation level read uncommitted
	set xact_abort on
	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	--	������������ � ��������� sql ��� ������� ������ �� ����� ������� � ������.
	--	��
	--		<table source="#contracts" destination="#c">
	--			<field source="contract_id" destination="cid"/>	
	--			<field source="isin"/>	
	--		</table>
	--	������� 
	--	insert into "#c"("cid", "isin") select "contract_id", "isin" from "#contracts"
	--	���� ��� ����-���������� �� �������, �� ��������� ������ ����� ����-���������.
	-----------------------------------------------------------------------------------------
	--	�������			��������� �������			24.01.2020
	--	������� ����������� ����������� ���������, ������� @order
	-----------------------------------------------------------------------------------------
	--	�������			�������� �������			10.02.2021
	--	��������� �� ��������� xml
	-----------------------------------------------------------------------------------------
	declare 
		 @xml nvarchar(max) = cast(@p_xml as varchar(max))
		,@sql varchar(max)


	declare
		 @source_table	varchar(max)
		,@dest_table	varchar(max)
		,@source_fields	varchar(max)
		,@order_fields	varchar(max)
		,@where			varchar(max)
		,@dest_fields	varchar(max)
		,@clear			varchar(max) = '1'

	-- ���� � ������ ���� ����� table - ���� �������� �� ������� - ��������, ����������, ������� �� ������� �������� � ��, ���������� � ��
	-- ���� � ������ ���� ����� field - ���� �������� ��� ������� - ��������, ����������, ���� �� ������ - �� ����� ���������
	select
		 @source_table	= isnull(source_table, @source_table)
		,@dest_table	= isnull(dest_table, @dest_table)
		,@order_fields	= isnull(order_fields, @order_fields)
		,@where			= isnull([where], @where)
		,@clear			= isnull(t.clear, @clear)
		,@source_fields = isnull(@source_fields, '') + isnull(', ' + source_fields,'')
		,@dest_fields	= isnull(@dest_fields, '') + isnull(', ' + dest_fields,'')
	from
	(
		select
			 src.src
			,dst.dst
			,source_table	= iif(charindex('table', value) > 0, iif(charindex('source', value) > 0, src.src, null), null)
			,dest_table		= iif(charindex('table', value) > 0, iif(charindex('destination', value) > 0, dst.dst, null), null)
			,order_fields	= iif(charindex('table', value) > 0, iif(charindex('order', value) > 0, ord.ord, null), null)
			,clear			= iif(charindex('table', value) > 0, iif(charindex('clear', value) > 0, clr.clr, null), null)
			,[where]		= iif(charindex('table', value) > 0, iif(charindex('where', value) > 0, whr.whr, null), null)
			,source_fields	= iif(charindex('field', value) > 0, '"' + src.src + '"', null)
			,dest_fields	= iif(charindex('field', value) > 0, '"' + dst.dst + '"', null)
			,value
		from
			string_split(@xml, '<') s
			outer apply (select start_s = substring(value, charindex('"', value, charindex('source', value)) + 1, len(value))) as start_source
			outer apply (select src = substring(start_source.start_s, 1, iif(charindex('"', start_source.start_s) > 1, charindex('"', start_source.start_s) - 1, 0))) as src
			outer apply (select start_d = substring(value, charindex('"', value, charindex('destination', value)) + 1, len(value))) as start_destination
			outer apply (select dst = substring(start_destination.start_d, 1, iif(charindex('"', start_destination.start_d) > 1, charindex('"', start_destination.start_d) - 1, 0))) as dst
			outer apply (select start_o = substring(value, charindex('"', value, charindex('order', value)) + 1, len(value))) as start_order
			outer apply (select ord = substring(start_order.start_o, 1, iif(charindex('"', start_order.start_o) > 1, charindex('"', start_order.start_o) - 1, 0))) as ord
			outer apply (select start_c = substring(value, charindex('"', value, charindex('clear', value)) + 1, len(value))) as start_clear
			outer apply (select clr = substring(start_clear.start_c, 1, iif(charindex('"', start_clear.start_c) > 1, charindex('"', start_clear.start_c) - 1, 0))) as clr
			outer apply (select start_w = substring(value, charindex('"', value, charindex('where', value)) + 1, len(value))) as start_where
			outer apply (select whr = substring(start_where.start_w, 1, iif(charindex('"', start_where.start_w) > 1, charindex('"', start_where.start_w) - 1, 0))) as whr
	) t

	select @source_fields = right(@source_fields, len(@source_fields) - 2)
	select @dest_fields = right(@dest_fields, len(@dest_fields) - 2)

	if @dest_table is not null
	begin
		if cast(@clear as int) = 1 
			select @sql = isnull(@sql, '') + 'delete ' + @dest_table + '; ' 

			select @sql = isnull(@sql, '') + 'insert into ' + @dest_table + '(' + @dest_fields + ') '
	end

	select @sql = isnull(@sql, '') + 'select ' + @source_fields + ' from ' + @source_table  + isnull(' where ' + @where, '') + isnull( ' order by ' + @order_fields , '')


	-- 4. ��������� SQL
	if @p_debug > 0
		print @sql
	if @p_debug < 2
		exec(@sql)

end

