USE msdb;

EXEC sp_help_jobhistory @job_name = '19:'
                      , @start_run_date = 20170901
                      , @end_run_date = 20171231
                      , @mode = 'FULL';