set transaction isolation level read uncommitted

if object_id('tempdb..#outdata') is not null
	drop table #outdata

declare
	@Persons table
					(
						 PersonID		int
						,Person_Name	varchar(100)
						,INN			varchar(20)
						,TinVat			varchar(50)
					)

insert into @Persons
(
	 PersonID
	,Person_Name
	,INN
	,TinVat
)
select 
	 PersonID
	,Person_Name
	,INN
	,TinVat = cast(
								case
									when Person_Name = 'VTB Capital PLC' then '480/63440/00081'
									when Person_Name = 'SIB (Cyprus) Ltd' then '12119924Q'
									when Person_Name = 'MeritKapital Ltd' then '12189316O'
									when Person_Name = 'Renaissance Securities (Cyprus) Limited' then '12072487T'
									when Person_Name = 'Omega Funds Investment Ltd' then '12230096M'
									else null
								end       
						as varchar(50))
from
	dbo.Persons
--where inn = '7709258228'


--select * from dbo.Persons PER where inn = '7709258228'

--select * from dbo.Audit_limit
--where dbo.Audit_limit.PersonID = 2175

----and dbo.Audit_limit.Audit_Limit_type_ID in (60,61)
--and dbo.Audit_limit.PersonID_2 = 461
--order by dbo.Audit_limit.beg_date



;with cteAuditLimitType as
(
	select
		 alt.Audit_limit_type_ID
		,alt.Audit_limit_type_name
		,alt.Audit_limit_type_code
	from
		dbo.Audit_Limit_type alt
	where
		alt.Audit_limit_type_code in ('L24', 'L36', 'L37', 'L44', 'L45')
)
, cteActualLimits as
(
	select
		 al.PersonID
		,beg_date = max(al.beg_date)
		,al.Audit_limit_type_ID
	from
		dbo.Audit_limit al
	where
		al.PersonID_2 = 461
	group by
		 al.PersonID
		,al.Audit_limit_type_ID
)
select
	 GroupContractor
	,NameContractor
	,INN
	,TypeContractor
	,DateStartStr = convert(varchar, max(DateStartStr), 104)
	,TinVat
	,REPOOut	= max([L36])
	,REPOIn		= max([L37])
	,DVP		= max([L44])
	,FOP		= max([L45])
	,Editing	= 0
	,Comment	= max(pvt.Comment)
into #outdata
from
(
	select
		 GroupContractor	= pgi.person_group_name
		,NameContractor		= p.Person_Name
		,INN				= p.INN
		,TypeContractor		= cast(null as varchar(255))
		,DateStartStr		= al.beg_date
		,TinVat				= p.TinVat
		,alt.Audit_limit_type_code
		,Value = al.Value / 1000000
		,al.Comment
	from
		cteActualLimits acl
		inner join dbo.Audit_limit al
			on	al.PersonID	= acl.PersonID
			and al.beg_date = acl.beg_date
			and al.Audit_limit_type_ID = acl.Audit_limit_type_ID
		inner join cteAuditLimitType alt
			on	 alt.Audit_limit_type_ID = al.Audit_limit_type_ID
		inner join @Persons p
			on	p.PersonID = al.PersonID
		left join dbo.Persons_group_ident pgi
			on	pgi.person_group_ident_id = al.person_group_ident_id
	where
		al.PersonID_2 = 461
) p
pivot
(
	sum(Value)
	for Audit_limit_type_code
	in
	([L24], [L36], [L37], [L44], [L45])
) as pvt
group by
	 GroupContractor
	,NameContractor
	,INN
	,TypeContractor
	,TinVat


select 
	 *
from 
	#outdata
order by 
	 GroupContractor
	,NameContractor