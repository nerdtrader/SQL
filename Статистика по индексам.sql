
DECLARE @ObjectName Nvarchar(255) = 'OperOperationsFact'
     
DECLARE @objid Int
SET @objid = OBJECT_ID(@ObjectName)

IF @objid IS NULL
BEGIN
    SELECT @objid = [object_id] FROM sys.tables WHERE NAME = @ObjectName  
END;

DECLARE @table_indexes table
(
    TableName           Nvarchar(255),
    IndexName           Nvarchar(255),
    IndexId             Nvarchar(255),
    data_space_id       int,
    GroupName           Nvarchar(255), 
    IColumns            Nvarchar(max),
    Includes            Nvarchar(max),
    Type_desc           Nvarchar(255), 
    is_unique           bit,
    is_primary_key      bit, 
    is_descending_key   bit,
    has_filter          bit, 
    filter_definition   Nvarchar(255),

    worked              bit default(0)
)

INSERT @table_indexes
(
    TableName,
    IndexName,
    IndexId,
    data_space_id,
    Type_desc,
    is_unique,
    is_primary_key,
    has_filter,
    filter_definition
)
    SELECT OBJECT_SCHEMA_NAME(object_id) + '.' + @ObjectName tablename,
           name,
           index_id,
           data_space_id,
           type_desc,
           is_unique,
           is_primary_key,
           has_filter,
           filter_definition
    FROM sys.indexes
    WHERE object_id = @objid


DECLARE @index_columns      nvarchar(max),
        @include_columns    nvarchar(max),
        @indid              int,
        @data_space_id      int,
        @GroupName          nvarchar(255)

WHILE exists (
    SELECT *
    FROM @table_indexes
    WHERE worked = 0
) BEGIN
    SELECT TOP 1 @indid = IndexId,
                 @data_space_id = data_space_id
    FROM @table_indexes
    WHERE worked = 0

    SET @index_columns = (
        SELECT (name + ';') AS 'text()'
        FROM sys.index_columns sic
        JOIN sys.columns sc
            ON sic.object_id = sc.object_id
            --AND si.index_id = 
            AND sic.column_id = sc.column_id
        WHERE sc.object_id = @objid
            AND sic.index_id = @indid
            AND sic.is_included_column = 0
        ORDER BY sic.key_ordinal
        FOR Xml PATH ('')
    );

    SET @include_columns = (
        SELECT (name + ';') AS 'text()'
        FROM sys.index_columns sic
        JOIN sys.columns sc
            ON sic.object_id = sc.object_id
            --AND si.index_id = 
            AND sic.column_id = sc.column_id
        WHERE sc.object_id = @objid
            AND sic.index_id = @indid
            AND sic.is_included_column = 1
        FOR Xml PATH ('')
    );

    SET @index_columns = REPLACE(SUBSTRING(@index_columns, 1, LEN(@index_columns) - 1), ';', ', ')
    SET @include_columns = REPLACE(SUBSTRING(@include_columns, 1, LEN(@include_columns) - 1), ';', ', ')

    SELECT @GroupName = name
    FROM sys.data_spaces
    WHERE data_space_id = @data_space_id

    UPDATE @table_indexes
        SET IColumns  = @index_columns,
            Includes  = @include_columns,
            GroupName = @GroupName,
            worked    = 1
    WHERE IndexId = @indid

END


SELECT t.TableName,
       t.IndexName,
       t.IndexId,
       LOWER(t.Type_desc) + CASE
           WHEN t.is_primary_key = 1 THEN ' primary key '
           ELSE ''
       END + CASE
           WHEN t.is_unique = 1 THEN ' unique '
           ELSE ''
       END                                        AS [type],
       t.IColumns + CASE
           WHEN indcol.is_descending_key = 1 THEN ' DESC'
           ELSE ' '
       END                                        AS [indexcolumns],
       t.Includes,
       size.*,
       stat.[user_seeks],
       stat.[user_scans],
       stat.[user_lookups],
       stat.[user_updates],
       stat.[last_user_seek],
       stat.[last_user_scan],
       stat.[last_user_lookup],
       stat.[last_user_update],
       compr.data_compression_desc                AS [data_compression],
       t.GroupName,
       ISNULL(t.filter_definition, 'No filtered') AS [filter]
FROM @table_indexes t
CROSS APPLY (
    SELECT SUM(row_count)                                                                                       AS [rows],
           SUM(ps.reserved_page_count) * 8 / 1024                                                               AS [reserved_mb],
           SUM(ps.in_row_data_page_count + ps.lob_used_page_count + ps.row_overflow_used_page_count) * 8 / 1024 AS [data_mb],
           SUM(ps.used_page_count) * 8 / 1024                                                                   AS [used_mb]
    FROM sys.dm_db_partition_stats ps
    WHERE ps.object_id = @objid
        AND ps.index_id = t.IndexId
) size
CROSS APPLY (
    SELECT *
    FROM sys.partitions sp
    WHERE sp.object_id = @objid
        AND sp.index_id = t.IndexId
) compr
CROSS APPLY (
    SELECT TOP 1 *
    FROM sys.index_columns sicl
    WHERE sicl.object_id = @objid
        AND sicl.index_id = t.IndexId
) indcol
OUTER APPLY (
    SELECT SUM(s.user_seeks)       AS [user_seeks],
           SUM(s.user_scans)       AS [user_scans],
           SUM(s.user_lookups)     AS [user_lookups],
           SUM(s.user_updates)     AS [user_updates],
           MAX(s.last_user_seek)   AS [last_user_seek],
           MAX(s.last_user_scan)   AS [last_user_scan],
           MAX(s.last_user_lookup) AS [last_user_lookup],
           MAX(s.last_user_update) AS [last_user_update]
    FROM sys.dm_db_index_usage_stats s
    WHERE s.database_id = DB_ID()
        AND s.object_id = @objid
        AND s.index_id = t.IndexId
) stat
ORDER BY compr.index_id
