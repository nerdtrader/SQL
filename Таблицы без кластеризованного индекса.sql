USE [rts]
SELECT OBJECT_SCHEMA_NAME(si.[object_id]) + '.' + OBJECT_NAME(si.[object_id]),
       si.[type_desc],
       sp.[rows]
FROM   sys.[indexes] si
       JOIN sys.[partitions] sp
            ON  sp.[object_id] = si.[object_id]
            AND sp.[index_id] = si.[index_id]
WHERE  si.[index_id] = 0
ORDER BY
       3 desc
