merge dbo.client_commissions CC
			using (
				select
					case
						when PT.is_subaccount = 1 then PT.client_id
						else C.manager
					end as client_id				
					,PT.[type_id]
					,dbo.fn_client_commission_period_begin(@date, PT.[type_id]) as [date]				
					,0 as base_commission					
					,0 as commissions_charged				
					,PT.comment				
					,PT.amount as commission_to_charge			
					,PT.source_stock_id			
					,PT.n_doc				
					,PT.contragent_id		
					,PT.services_date
					,PT.write_off_stock_id			
					,PT.write_off_stock_type_id	
					,PT.is_subaccount									
					,PT.contract_id
					,PT.service_id
					,PT.parameter_id
					,PT.analytic_1
					,PT.analytic_2		
					,PT.analytic_3
					,PT.analytic_4
					,PT.currency_id
					,PT.external_id
					,PT.assets
					,PT.tariff_plan
				from
					#planned_trans PT
					inner join dbo.client C
						on C.client_id = PT.client_id
				) S
				on CC.id = 0
			when not matched then
				insert (
					client_id
					,[type_id]
					,[date]
					,base_commission
					,commissions_charged
					,comment
					,commission_to_charge
					,stock_id
					,n_doc
					,contragent_id	
					,services_date
					,write_off_stock_id
					,write_off_stock_type_id
					,is_subaccount
					,contract_id
					,service_id
					,parameter_id
					,analytic_1
					,analytic_2
					,analytic_3
					,analytic_4
					,currency_id
					,assets
					,tariff_plan
					,source_proc_name
				) values (
					S.client_id
					,S.[type_id]
					,S.[date]
					,S.base_commission
					,S.commissions_charged
					,S.comment
					,S.commission_to_charge
					,S.source_stock_id
					,S.n_doc
					,S.contragent_id		
					,S.services_date
					,S.write_off_stock_id
					,S.write_off_stock_type_id
					,S.is_subaccount
					,S.contract_id
					,S.service_id
					,S.parameter_id
					,S.analytic_1
					,S.analytic_2
					,S.analytic_3
					,S.analytic_4
					,S.currency_id
					,S.assets
					,S.tariff_plan
					,@p_source_proc_name		
				)
			output
				inserted.id
				,S.external_id
			into #client_commissions_inserted (
				id
				,external_id
			);
