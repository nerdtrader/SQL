;WITH requests
AS
(
    SELECT 
        es.session_id
    ,   rgwg.[name] AS group_name
    ,   er.blocking_session_id
    ,   COALESCE(UPPER(esb.[status]), N'') AS blocking_session_status
    ,   es.[host_name]
    ,   es.[program_name]          
    ,   es.login_name
    ,   es.memory_usage * 1. / 128 AS memory_usage_mb
    ,   DB_NAME(er.database_id) AS [database_name]
    ,   QUOTENAME(OBJECT_SCHEMA_NAME(est.objectid, est.[dbid])) + N'.' +
             QUOTENAME(OBJECT_NAME(est.objectid, est.[dbid])) AS [object_name]
    ,   er.start_time
    ,   es.last_request_start_time
    ,   er.is_resumable
    ,   er.percent_complete
    ,   CONVERT(time, DATEADD(SECOND, er.estimated_completion_time / 1000, 0)) AS estimated_completion_time
    ,   CONVERT(time, DATEADD(SECOND, er.total_elapsed_time / 1000, 0)) AS elapsed_time
    ,   CONVERT(time, DATEADD(SECOND, er.cpu_time / 1000, 0)) AS cpu_time
    ,   er.logical_reads
    ,   er.reads AS physical_reads
    ,   er.writes
    ,   UPPER(er.[status]) AS [status]      
    ,   CASE
        WHEN er.wait_type IS NULL THEN '*' + er.last_wait_type
        ELSE er.wait_type
        END AS wait_type
    ,   er.wait_resource
    ,   CONVERT(time, DATEADD(MILLISECOND, er.wait_time, 0)) AS wait_time
    ,   er.command
    ,   CASE es.transaction_isolation_level
        WHEN 0 THEN 'Unspecified'
        WHEN 1 THEN 'ReadUncomitted'
        WHEN 2 THEN 'ReadCommitted'
        WHEN 3 THEN 'Repeatable'
        WHEN 4 THEN 'Serializable'
        WHEN 5 THEN 'Snapshot'
        END AS session_transaction_isolation_level_name
    ,   CONVERT(time, DATEADD(MILLISECOND, es.[lock_timeout] / 1000, 0)) AS session_lock_timeout    
    ,   es.[deadlock_priority] AS session_deadlock_priority
    ,   CASE er.transaction_isolation_level
        WHEN 0 THEN 'Unspecified'
        WHEN 1 THEN 'ReadUncomitted'
        WHEN 2 THEN 'ReadCommitted'
        WHEN 3 THEN 'Repeatable'
        WHEN 4 THEN 'Serializable'
        WHEN 5 THEN 'Snapshot'
        END AS request_transaction_isolation_level_name
    ,   CONVERT(time, DATEADD(SECOND, er.[lock_timeout] / 1000, 0)) AS request_lock_timeout
    ,   er.[deadlock_priority] request_deadlock_priority
    ,   er.open_transaction_count AS request_open_transaction_count
    ,   CASE 
        WHEN LEN(est.text) < (er.statement_end_offset / 2) + 1 THEN est.text
        WHEN SUBSTRING(est.text, (er.statement_start_offset / 2), 2) LIKE N'[a-zA-Z0-9][a-zA-Z0-9]' THEN est.text
        ELSE
                 CASE
                    WHEN er.statement_start_offset > 0 THEN
                           SUBSTRING
                           (
                                  est.text
                           ,   ((er.statement_start_offset / 2) + 1)
                           ,   CASE
                                  WHEN er.statement_end_offset = -1 THEN 2147483647
                                  ELSE ((er.statement_end_offset - er.statement_start_offset)/2) + 1
                                  END                                     
                        )
                 ELSE est.text
                 END
        END AS query_text    
    ,   'SELECT 
    CONVERT(xml, query_plan) AS query_plan  
FROM
    sys.dm_exec_query_plan(' + CONVERT(nvarchar(MAX), er.plan_handle, 1) + ');' AS query_plan
          ,'SELECT
    CONVERT(xml, query_plan) AS query_plan  
FROM
    sys.dm_exec_text_query_plan(' + 
            CONVERT(nvarchar(MAX), er.plan_handle, 1) + ', ' + 
            CONVERT(nvarchar(MAX), er.statement_start_offset) + ', ' + 
            CONVERT(nvarchar(MAX), er.statement_end_offset) + ');' AS query_plan_request
    ,   N'D:\PS\Get-QueryPlan.ps1 -ConnectionString "Server=' 
        + @@SERVERNAME + ';Trusted_Connection=True;" -PlanHandle "' 
        + CONVERT(NVARCHAR(MAX), er.plan_handle, 1) + N'" -StartOffset ' 
        + CONVERT(NVARCHAR(MAX), er.statement_start_offset) + N' -EndOffset ' 
        + CONVERT(NVARCHAR(MAX), er.statement_end_offset) + N' -FilePach "D:\Plans\'
        + CONVERT(NVARCHAR(MAX), NEWID()) + N'.sqlplan"' AS query_plan_send_to_file
    ,   CASE
        WHEN LEFT(es.[program_name], 8) = 'SQLAgent' THEN 
             N'SELECT 
    name 
FROM
    msdb..sysjobs WITH (NOLOCK) 
WHERE
    SUBSTRING(''' + es.program_name + ''', 32, 32) = SUBSTRING(sys.fn_varbintohexstr(job_id), 3, 100);'
           END AS job_name
    ,   (
           SELECT 
               esws.wait_type
            ,   esws.waiting_tasks_count
            ,   esws.wait_time_ms
            ,   esws.max_wait_time_ms
            ,   esws.wait_time_ms - esws.signal_wait_time_ms AS resource_wait_time_ms -- Waiter list (SUSPENDED).
            ,   esws.signal_wait_time_ms -- Runnable queue (RUNNABLE).
            ,   esws.wait_time_ms AS total_wait_time_ms
            ,   COALESCE(esws.wait_time_ms / esws.waiting_tasks_count, 0) AS avg_wait_time_ms        
           FROM
               sys.dm_exec_session_wait_stats AS esws
           WHERE
               esws.session_id = es.session_id
            ORDER BY 
                wait_time_ms DESC
            FOR XML RAW ('wait'), TYPE
        ) AS session_wait_stats
    ,   ib.event_info AS input_buffer          
    FROM   
        sys.dm_exec_requests AS er WITH (NOLOCK)
        INNER JOIN sys.dm_exec_sessions AS es WITH (NOLOCK)
            ON er.session_id = es.session_id         
        LEFT JOIN sys.resource_governor_workload_groups AS rgwg WITH (NOLOCK)
            ON es.group_id = rgwg.group_id
        LEFT JOIN sys.dm_exec_sessions AS esb
            ON er.blocking_session_id = esb.session_id
        OUTER APPLY sys.dm_exec_input_buffer(es.session_id, er.request_id) AS ib
        OUTER APPLY sys.dm_exec_sql_text(er.[sql_handle]) AS est           
    WHERE  
        es.is_user_process = 1
        AND es.session_id <> @@SPID
)

SELECT 
    session_id
,   group_name
,   blocking_session_id
,   blocking_session_status
,   start_time
,   last_request_start_time
,   login_name
,   [status]
,   command
,   CASE 
    WHEN [program_name] LIKE 'SQLAgent%' THEN N'*' + query_text 
    else query_text
    END AS query_text
,   [database_name]
,   [object_name]
,   elapsed_time
,   cpu_time
,   logical_reads
,   physical_reads
,   writes
,   is_resumable
,   percent_complete
,   estimated_completion_time
,   wait_type
,   wait_resource
,   wait_time
,   memory_usage_mb
,   [host_name]
,   [program_name]
,   job_name
/*
,   session_transaction_isolation_level_name
,   session_lock_timeout
,   session_deadlock_priority
,   session_open_transaction_count             
,   request_transaction_isolation_level_name
,   request_lock_timeout
,   request_deadlock_priority
,   request_open_transaction_count
*/
,   input_buffer      
,   query_plan -- .sqlplan
,   query_plan_request
,   query_plan_send_to_file
,   session_wait_stats      
FROM   
    requests
WHERE
       1 = 1
ORDER BY 
    elapsed_time DESC
OPTION (RECOMPILE);
GO
