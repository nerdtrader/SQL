SELECT error_time
      ,client_app_name
      ,dtbase_name
      ,usr_name
      ,error_texts
      ,procedure_id
      ,database_id
      ,proc_name
      ,load_date
FROM   dbo.std_errors_table
WHERE  procedure_id<>2058161052
ORDER BY
       error_time DESC